<?php

function custom_required_message_field_info_alter(&$info){
  foreach ($info as $key => &$value) {
    $value['required_error'] = '';
  }
}

function custom_required_message_form_field_ui_field_edit_form_alter(&$form, $form_state){
  $descriptoin = array();
  $descriptoin[] = t('Leave blank to use the default core messages. You can use tokens !field_name, %field_name and @field_name as well as other tokens');
  
  if(in_array($form['#field']['type'], array('datetime', 'date', 'datestamp'))){
    $descriptoin[] = 'You can define messages for start and end date by separating them by a pipe (|). If only one message is defined, it will be shown for both error messages.';
    if(module_exists('clientside_validation')){
      $descriptoin[] = t('For Date select list use tokens !date_part, %date_part and @date_part (<strong class="warning">Warning:</strong> works only for clientside validation, don\'t use these tokens withot cliendside validation because they will not be replaced).');
    }
    
  }
  
  $descriptoin = t(implode(' ', $descriptoin));
  
  $form['instance']['required_error'] = array(
    '#type' => 'textfield',
    '#title' => t('Required error message'),
    '#description' => $descriptoin,
    '#default_value' => isset($form['#instance']['required_error']) ? $form['#instance']['required_error'] : FALSE,
    '#weight' => -9.9,
    '#states' => array(
      'visible' => array(
        ':input[name="instance[required]"]' => array('checked' => TRUE)
      ) 
    )
  );
}