<?php

/**
 * Implements hook_i18n_object_info_alter().
 */
function custom_required_message_i18n_object_info_alter(&$info){
  
  $info['field_instance']['string translation']['properties']['required_error'] = array(
    'title' => t('Required error message'),
//     'field' => 'settings.required_error',
  );
  
}
